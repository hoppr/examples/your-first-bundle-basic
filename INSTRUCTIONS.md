
Welcome to the `Your First Bundle - Basic` example!

We recommend reading through the walkthrough on hoppr.dev.

- https://hoppr.dev/docs/get-started/your-first-bundle 

To run this example execute the following commands in the terminal below.

> cd test-hoppr
>
> hopctl bundle my-first-manifest.yml --transfer my-first-transfer.yml
