![Hoppr Examples](https://gitlab.com/hoppr/hoppr/-/raw/dev/media/hoppr-repo-banner.png)

# Your First Bundle - Basic Example

This is the [Your First Bundle - Basic](https://hoppr.dev/docs/get-started/your-first-bundle) example from [hoppr.dev](https://hoopr.dev).
[This example](https://hoppr.dev/docs/get-started/your-first-bundle) walks you through the input files and the output created. 

If you are using `hoppr` for the first time, try running it in [gitpod](https://gitpod.io/#https://gitlab.com/hoppr/examples/your-first-bundle-basic/-/tree/main/)
so there is no local setup required. This will setup an environment and allow you to run it remotely.

- [Run on gitpod to avoid setup!](https://gitpod.io/#https://gitlab.com/hoppr/examples/your-first-bundle-basic/-/tree/main/)
